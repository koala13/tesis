# Mide Temperatura con RTD (2 pines) --> import max31865
# Regula intensidad de LED con un pote 10K externo + vumetro que indica intensidad + boton que maneja interrupciones) --> import intLed
# Regula tension de una fuente con XL4016 y MCP4131-10K  --> import fuenteXL4016
# Las 3 funciones anteriores fueron implementadas como objetos

# Falta guardar las mediciones de temperatura y tiempo en un archivo

import max31865
import intLed
import fuenteXL4016
import gpio as GPIO
from time import sleep
from machine import Pin


# Definicion de Pines para medicion de temperatura para MAX31865
csMaxPin = 17                               # SPIO cs para MAX
misoMaxPin = 16                             # miso = SPIO Tx para MAX
mosiMaxPin = 19                             # mosi = SPIO Rx para MAX
clkMaxPin = 18                              # SPIO clk para MAX

# Definicion de Pines para intensidad de Led y Vumetro
pwmLedPin = 15                              # SPIO PWM para LED
adcLedPin = 26                              # SPIO ADC para LED
frecuenciaPwm = 1000                        # Seteo de frecuencia del PWM
led_rojo = Pin(13, Pin.OUT)
led_amarillo = Pin(12, Pin.OUT)
led_verde = Pin(11, Pin.OUT)
boton = Pin(10, Pin.IN, Pin.PULL_DOWN)

# Difinicion de Pines para control de Vsalida, fuente XL4016
csFuentePin = 5                              # SPIO cs para Fuente
clkFuentePin = 2                             # SPIO clk para Fuente
mosiFuentePin = 4                            # mosi = SPIO Rx para Fuente



###############################################################################
# Inicializa los 3 leds del vumetro: Apagados
led_rojo.value(0)
led_amarillo.value(0)
led_verde.value(0)

#intensidad = 32515       # 0 apagado, 65025 maximo prendido
#print ('Intensidad:', round((intensidad*100/65025),2) ,'%')

# Manejo de interrupciones: al apretar el boton, parpadean los 3 Leds
def int_handler(pin):
    boton.irq(handler=None)
    print ('Blinking')
    led_rojo.value(0); led_amarillo.value(0); led_verde.value(0)
    sleep (0.5)
    led_rojo.toggle(); led_amarillo.toggle(); led_verde.toggle()
    sleep (0.5)
    led_rojo.toggle(); led_amarillo.toggle(); led_verde.toggle()
    sleep (0.5)
    led_rojo.toggle(); led_amarillo.toggle(); led_verde.toggle()
    sleep (0.5)
    led_rojo.toggle(); led_amarillo.toggle(); led_verde.toggle()
    sleep (2)
    boton.irq(handler=int_handler)

boton.irq(trigger=Pin.IRQ_RISING, handler=int_handler)


intensidad = 0                                          # intensidad inicial: 0 = apagado
#resistencia = 70                                        # valor de R del potenciomentro, en Ohms
#print ('Resistencia: ' + str(resistencia) + ' Ohms')    # imprime el valor de resistencia seteado  
#level = int(resistencia*128/10000)                      # traduccion a bits internos: 0 = 0 ohm, 127 = 10K Ohm
  

###############################################################################
# Inicializa medidor de temperatura Max31865
max = max31865.max31865(csMaxPin,misoMaxPin,mosiMaxPin,clkMaxPin)

# Inicializa LED al cual se le regulará su intensidad
LED = intLed.intLed(intensidad,pwmLedPin,adcLedPin,frecuenciaPwm)

# Inicializa Fuente XL4016
fuente = fuenteXL4016.fuenteXL4016(csFuentePin,mosiFuentePin,clkFuentePin)

 
###############################################################################
while True:
    LED.potencia = LED.adc.read_u16()       
    LED.pwm.duty_u16(LED.potencia)
    print ('Intensidad del Led:', round((LED.potencia*100/65025),2) ,'%')
    if LED.potencia < round(65025/4):
        led_rojo.value(0)
        led_amarillo.value(0)
        led_verde.value(0)
    if (LED.potencia > round(65025/4)) and (LED.potencia < round(65025*2/4)):
        led_rojo.value(0)
        led_amarillo.value(0)
        led_verde.value(1)
    if (LED.potencia > round(65025*2/4)) and (LED.potencia < round(65025*3/4)):
        led_rojo.value(0)
        led_amarillo.value(1)
        led_verde.value(1)
    if LED.potencia > round(65025*3/4):
        led_rojo.value(1)
        led_amarillo.value(1)
        led_verde.value(1)
    sleep (0.2)

    resistencia = 196                                                       # valor de R que se quiere en el potenciomentro, en Ohms
    print ('Resistencia esperada: ' + str(resistencia) + ' Ohms')           # imprime el valor de resistencia que se quiere
    level = round(resistencia*128/10000)                                    # traduccion a bits internos: 0 = 0 ohm, 127 = 10K Ohm. Redondea al valor mas cercano posible
    print ('Resistencia configurada: ' + str(level*10000/128) + ' Ohms')    # imprime el valor de resistencia seteado  
    print ('Nivel [0-127]: ', level)                                        # imprime el nivel [0-127] que se configura en el pote
    fuente.set_value(level)                                                 # Setea el valor de Resistencia en el pote digital
    sleep(0.1)
    GPIO.cleanup()
#    sleep (1)
  
    
    tempC = max.readTemp()                                  # Mide temperatura e imprime en pantalla, junto con otros valores
    sleep (0.5)   
    GPIO.cleanup()
   
###############################################################################
