# Controlador de temperatura para Skipper-CCD

## Led+Temp+Fuente.py
Mide Temperatura con RTD (2 pines) --> import max31865

Regula intensidad de LED con un pote 10K externo + vumetro que indica intensidad + boton que maneja interrupciones --> import intLed

Regula tension de una fuente con XL4016 y MCP4131-10K  --> import fuenteXL4016

Las 3 funciones anteriores fueron implementadas como objetos

## fuenteXL4016.py
Objeto fuenteXL4016 que se utiliza para regular la tensión de salida de una fuente switching basada en el XL4016, a traves de una R variable de 10K Ohm (digital), usando pines de control de la Raspberry Pi Pico

## intLed.py
Objeto intLed que se utiliza para regular la intensidad de un Led usando el PWM y el ADC de la Raspberry Pi Pico

## gpio.py
Objeto gpio que se utiliza para configurar los puertos de la Raspberry Pi Pico

## max31865.py
Objeto max31865 que se utiliza para controlar un sensor de temperatura

## calibracion_fuente.py
Permite calibrar los ADC para medir V o I. Se configura la cantidad de puntos a medir (M) y la cantidad de mediciones por punto (N), y promedia. Devuelve un array con los valores de V e I (transformados de los ADC), para cada punto M. Eso luego se ingresa manualmente en calibracion_fuente_graficos.py

## calibracion_fuente_graficos.py
Con los valores de calibracion_fuente.py y las mediciones con multimetro se ajusta por polinomio de grado definible por usuario y devuelve los parametros de ajuste, junto con los graficos (en "y" lo medido con multimetro y en "x" lo obtenido con ADC). Luego los parametros de ajuste se ingresaran manualmente en el codigo principal (main).

## main_test12.py
Este codigo se instala en la Raspberry Pi Pico. Permite recibir comandos por terminal (con codigo test12.py).
Tiene funciones de medicion de Temperatura (con Max31865), medicion de potencia (con ADC0 y 1, y sus ajustes configurados a mano, con calibracion_fuente_graficos.py), setea intensidad y tiempo de encendido de un LED, flashea un led ("x" mseg), y muestra todo en OLED.

## test12.py
Funciona en conjunto con main_test12.py: permite ingresar comandos por consola (port serial), y controlar a la Raspberry Pi Pico sin la necesidad de uso de Thonny.

## max31865_5.py
Libreria que se usa en main_test12.py (distinta a max31865.py). Mide Temperatura y devuelve su valor junto con el RTD.

## display1306_Test3.py
Ejemplo de programacion de OLED 1306.

## mcp41HV51-502_Test4.py
Ejemplo de manejo de potenciometro digital MCP41HV51-502 (5K, 256 pasos). Tambien mide V e I con ADC0 y 1 (corriente con sensor de Efecto Hall ACS712).

## Comandos de control de GIT
```
git clone git@gitlab.com.....
cd tesis
git status
git add Test.py
git commit -a -m "Comentario"
git push origin main
git branch Fede
git checkout main
git merge Fede
```
