# Este codigo permite calibrar la medicion de Tension y Corriente entregada por la fuente (con pote digital de 5K - MCP41HV51_502),
# medida a traves de los ADC0 (GP26, Tension) y ADC1 (GP27, Corriente) de la Raspberry. Para la corriente tambien se usa un
# sensor de efecto Hall (ACS712)

# La calibracion se hace repitiendo las mediciones de tension y corriente N veces con el ADC, para M puntos, y promediando. 
# Luego se comparará con la medicion con tester y se ajustará con un polinomio (con el codigo calibracion_fuente_graficos.py)
# Esa curva, finalmente se ingresa (a mano) en el codigo que mide Temperatura, FlashLed, Potencia, etc

import time
import gpio as GPIO                     # Es necesario contar con la libreria gpio
from machine import ADC, Pin


# Configuracion de los pines de control de la Raspberry Pi Pico
SPI_CS_PIN  = 9         # SPIO Cs
SPI_CLK_PIN = 6        # SPIO CLK
SPI_SDI_PIN = 8        # mosi = SPIO Rx
SPI_SDO_PIN = 7        # miso = SPIO Tx (no se usa)
shutdown = Pin(0, Pin.OUT, Pin.PULL_DOWN)      # Pin de control de apagado del MCP
shutdown.value(1)                              # inicializo el MCP (prendido)

adc0 = (Pin(26, Pin.IN))                       # agregado para configurar GP en alta impedancia
adc0 = ADC(Pin(26))                            # configuracion de Pin para medicion de Tension
adc1 = (Pin(27, Pin.IN))                       # agregado para configurar GP en alta impedancia
adc1 = ADC(Pin(27))                            # configuracion de Pin para medicion de Corriente

# Configuracion de los puertos del Raspberry Pi Pico usando la libreria GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(SPI_CS_PIN, GPIO.OUT)
GPIO.setup(SPI_CLK_PIN, GPIO.OUT)
GPIO.setup(SPI_SDI_PIN, GPIO.OUT)
GPIO.setup(SPI_SDO_PIN, GPIO.IN)     # no se usa

# Funcion que setea el valor de resistencia que se quiere configurar en el potenciometro digital
# En "value" se pasa el valor de R que se necesita: 0 = 0 ohm, 256 = 5K Ohm
def set_value(value):
    GPIO.output(SPI_CS_PIN, True)
    GPIO.output(SPI_CLK_PIN, False)
    GPIO.output(SPI_CS_PIN, False)
    b = '{0:016b}'.format(value)
    for x in range(0, 16):
#        print ('x:' + str(x) + ' -> ' + str(b[x]))
        GPIO.output(SPI_SDI_PIN, int(b[x]))
        GPIO.output(SPI_CLK_PIN, True)
        GPIO.output(SPI_CLK_PIN, False)
    GPIO.output(SPI_CS_PIN, True)


# el divisor de tension tanto para medir tension como corriente, se calcula con la relacion de resistencias
# pero habria que medirlo, porque sino no se consideran posibles resistencias parasitas, entonces
# difiere lo medido con el tester vs. lo medido con la RP 
divisor_V = 2000 / (10000 + 2000)              # divisor de tension para la medicion de Tension (Vout = Vin . R1/R1+R2)
conversion_mV = 185                            # mV por Ampere segun datasheet de ACS712

# tension de salida del sensor cuando no circula corriente
DEFAULT_OUTPUT_VOLTAGE = 3.3/2  # sensor Vcc = 5 V, but a voltage divider is used to get 1.65 V from 2.5 V for the sensor out pin

N = 200                          # cantidad de veces que se mide
M = 8                           # cantidad de puntos de medicion, para ajustar la curva (multiplo divisor de 256)
valor_V_ADC = []                # lista que acumula los valores promedios las tensiones medidas para todos los puntos
valor_I_ADC = []                # lista que acumula los valores promedios las corrientes medidas para todos los puntos


print('MEDIDION DE TENSION CON MULTIMETRO')
print('**********************************')
print('Mida tension en los terminales J1.', M, 'valores, cada 5 segundos...')

for level in range (0, 256, int(256/M)):
    set_value(level)                    # configura el pote con el los valores que quiero medir (de 0 a 255)
    time.sleep(5)

print('MEDIDION DE CORRIENTE CON MULTIMETRO')
print('**********************************')
print('Mida tension en los terminales J2.', M, 'valores, cada 5 segundos...')

for i in range (5):
    time.sleep(1)
    print ('Preparese para medir Corriente en ' + str(5-i) + ' segundos')



for level in range (0, 256, int(256/M)):
    tensiones  = []                     # lista vacia donde se guardaran las N mediciones de tension con el ADC0
    corrientes = []                     # lista vacia donde se guardaran las N mediciones de corriente con el ADC1
    set_value(level)                    # configura el pote con el los valores que quiero medir (de 0 a 255)
    time.sleep(0.1)
    for i in range (N):
        tensiones.append(adc0.read_u16())
        corrientes.append(adc1.read_u16())
        time.sleep_ms(5)
    prom_tensiones = sum(tensiones)/len(tensiones)
    valor_tension_adc0 = prom_tensiones * 3.3 / 65535               # traduce la lectura del ADC0 a Tension (en Volts)
    valor_tension_fuente = (valor_tension_adc0 / divisor_V)         # traduce la tension del ADC0 a Tension de la Fuente
    valor_V_ADC.append(valor_tension_fuente)
    prom_corrientes = sum(corrientes)/len(corrientes)  
    valor_tension_adc1 = prom_corrientes * 3.3 / 65535              # traduce la lectura del ADC1 a Tension (en Volts)
    valor_tension_adc1 = ((valor_tension_adc1 - DEFAULT_OUTPUT_VOLTAGE) * 1000)    # convierte a mV
    valor_corriente_fuente = abs(valor_tension_adc1 / conversion_mV)   # calcula la corriente (en Ampere: 1A = 185 mV) a partir de la tension en mV    
    valor_I_ADC.append(valor_corriente_fuente)


shutdown.value(0)                                                   # apaga la fuente (la pone al minimo)
print ('Tensiones:',  valor_V_ADC)                                  # imprime los valores de tension promedio para cada punto
print ('Corrientes:', valor_I_ADC)                                  # imprime los valores de tension promedio para cada punto

