# Este codigo grafica las curvas de calibracion de los instrumentos (V e I), para M puntos (funciona en conjunto con el codigo
# calibracion_fuente.py) 
# Se hizo un ajuste por un polinomio de grado 1, o 2, o 3...: y = a0 * x^2 + a1 * x + a2
# Las ecuaciones resultantes del ajuste (para V e I), a traves de sus parametros, luego se ingresan manualmente en el codigo
# que mide Temperatura, FlashLED, Potencia, etc


import matplotlib.pyplot as plt
import numpy as np
#import pandas as pd

# ingresar manualmente los valores del tester y la salida de calibracion_fuente.py
valor_V_ADC    = np.array([2.054, 4.261, 6.547, 8.801, 11.104, 13.371, 15.702, 18.010])     # completar con los valores promedios de las tensiones medidas para todos los puntos
valor_V_tester = np.array([1.86, 4.07, 6.31, 8.56, 10.81, 13.08, 15.35, 17.65])             # completar con los M valores de tension medidos con el tester
valor_I_ADC    = np.array([0.048, 0.099, 0.244, 0.390, 0.534, 0.680, 0.828, 0.974])         # completar con los valores promedios de las corrientes medidas para todos los puntos
valor_I_tester = np.array([0.18, 0.40, 0.62, 0.84, 1.07, 1.29, 1.52, 1.74])                 # completar con los M valores de corriente medidos con el tester

# Carga de datos para graficar
#print('Cargando datos...')

#DatosTempEnf = np.genfromtxt('Mediciones_y_Graficos/18.05.2022_Curva_de_Enfriamiento/Temperaturas_Enfriamiento_06-04-2022.csv', delimiter=';' )
#DATOS_todos = np.loadtxt(rootMediciones+'listFiles.txt',dtype=str)

#data_tiempo = (DatosTempEnf[1:38000,0] - DatosTempEnf[1,0]) / (60*60)            # se divide por 60*60 para transformar segundos a horas
#data_temperatura = DatosTempEnf[1:38000,1]

#data_tiempo = (DatosTempEnf[1:25000,0] - DatosTempEnf[1,0]) / (60*60)            # se divide por 60*60 para transformar segundos a horas
#data_temperatura = DatosTempEnf[1:25000,1] + 273.15                              # para ver temperatura en Kelvin

grado_poli_V = 1                                                         # grado del polinomio de ajuste para TENSION
grado_poli_I = 1                                                         # grado del polinomio de ajuste para CORRIENTE

parametros_V = np.polyfit(valor_V_ADC,valor_V_tester, grado_poli_V)      # ajusto TENSION con polinomio de grado_poli_V (tercer parametro)
parametros_I = np.polyfit(valor_I_ADC,valor_I_tester, grado_poli_I)      # ajusto CORRIENTE con polinomio de grado_poli_I (tercer parametro)
modelo_x_V   = np.linspace(start=0, stop=20, num=1000)          # vector para las coordenadas x del modelo obtenido (para TENSION)
modelo_x_I   = np.linspace(start=0, stop=1.1, num=1000)           # vector para las coordenadas x del modelo obtenido (para CORRIENTE)
modelo_y_V   = np.polyval(parametros_V , modelo_x_V )           # calcula las coordanas Y usando los parámetros ajustados (para TENSION)
modelo_y_I   = np.polyval(parametros_I , modelo_x_I )           # calcula las coordanas Y usando los parámetros ajustados (para CORRIENTE)


# Graficamos TENSION: datos_x , datos_y
plt.figure(1)
plt.plot(valor_V_ADC, valor_V_tester, '.', label='Mediciones')
plt.plot(modelo_x_V, modelo_y_V, '-', label='Modelo ajustado: y = 0,99 x - 0,16', color='red')
plt.legend(loc='best')
plt.xlabel('Tension con ADC [V]')
plt.ylabel('Tension con multimetro [V]')
plt.title('CURVA DE AJUSTE DE TENSION')
plt.grid()
plt.show()
plt.tight_layout()

plt.savefig('calibracion_fuente_V_25-05-2022_v2.jpg')


# Graficamos CORRIENTE: datos_x , datos_y
plt.figure(2) 
plt.plot(valor_I_ADC, valor_I_tester, '.', label='Mediciones')
plt.plot(modelo_x_I, modelo_y_I, '-', label='Modelo ajustado: y = 1,61 x + 0,19', color='red')
plt.legend(loc='best')
plt.xlabel('Corriente con ADC [A]')
plt.ylabel('Corriente con multimetro [A]')
plt.title('CURVA DE AJUSTE DE CORRIENTE')
plt.grid()
plt.show()
plt.tight_layout()

plt.savefig('calibracion_fuente_I_25-05-2022_v2.jpg')


# En el vector parametros están guardados los parametros optimos hallados, ordenados desde el mayor orden hasta el menor
print('Polinomio de ajuste para TENSION:')
print('modelo_y_V = parametros_V[0] * modelo_x_V + parametros_V[1]')
for i,parametro in enumerate(parametros_V):
    print ('parametro', i, ': ', parametro)

print('***********************************')
print('Polinomio de ajuste para CORRIENTE:')
print('modelo_y_I = parametros_I[0] * modelo_x_I^2 + parametros_I[1] * modelo_x_I + parametros_I[2]')
for i,parametro in enumerate(parametros_I):
    print ('parametro', i, ': ', parametro)