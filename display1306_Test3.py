# Manejo de Display OLED SSD1306 (128x32) I2C
# Paginas utiles con ejemplos:
# https://docs.micropython.org/en/latest/esp8266/tutorial/ssd1306.html
# https://www.youtube.com/watch?v=Zy64kZEM_bg
# https://www.youtube.com/watch?v=YR9v04qzJ5E
# https://www.youtube.com/watch?v=_pXXfXHSccc

import time
import gpio as GPIO                          # Es necesario contar con la libreria gpio
from machine import Pin, I2C
from time import sleep
from ssd1306 import SSD1306_I2C              # sirve para el I2C del display (hay otra libreria para manejarlo por SPI)
import framebuf                              # sirve para transformar una imagen en bytes

# Configuracion de los pines de control de la Raspberry Pi Pico
I2C_SDA_PIN = machine.Pin(20)               # Pin SDA de RP Pi (I2C0, pin 26, GP20)
I2C_SCL_PIN = machine.Pin(21)               # Pin SCL de RP Pi (I2C0, pin 27, GP21)

# Dimensiones del Display y Logo
WIDTH       = 128
HEIGHT      = 32
WIDTH_LOGO  = 115
HEIGHT_LOGO = 32

# Inicializa I2C y OLED SSD1306
i2c = machine.I2C(0, sda=I2C_SDA_PIN, scl=I2C_SCL_PIN, freq=300000)     # I2C 0, sda pin, scl pin, freq (entre 200K y 400K)
oled = SSD1306_I2C(WIDTH, HEIGHT, i2c)

# Logo: hay que convertir a bytes el jpg con el logo de X x Y pixeles
buffer = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1e@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1e@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x7f\xfc\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\xe0>\x00\xf0\x01\xe0x<\x7f\xc3\xfe\x03\xc0\x00\x07\x80\x03\x00p\x01\xe0\xf8|\x7f\xe3\xff\x03\xc0\x00\x0c\x04\x01\xc0\xc0\x03\xf0~|`c\x07\x87\xe0\x00\x0c\x04\x01\xc0\xc0\x03\xf0~|`c\x07\x87\xe0\x00\x0c\x1e\x00\xe0p\x03\xb0x|`a\x81\x87`\x00\x18\x07\x80\xe0\xf0\x03\xb0\xf6L\x7f\xc3\x01\x87`\x00\x18\x03\x800p\x06\x1cv\xcc\x7f\xc3\x81\x8c`\x00\x18\x03\x800p\x06\x1cv\xcc\x7f\xc3\x81\x8c`\x00\x10\x07\x800\xc0\x07\xbcv\xcc`c\x01\x8f8\x00p\x07\xc00p\x07\xfc\xf7\xcc`c\x07\x8f\xf8\x00\x10\x1f\xc00\xf0\x1e.w\x8ca\xe3\x97<\xbc\x00p\x1d\xe00\x7f\xdc\x0eq\x8c\x7f\xc3\xff8\x1c\x00p\x1d\xe00\x7f\xdc\x0eq\x8c\x7f\xc3\xff8\x1c\x00\x18<`0KX\x0c\x91\x08Z\x00\x900\x18\x00\x188` \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18x~\xe0\x00\x00\x00\x00\x00\x00\x00\x00@\x00\x00\x0c\xe0>\xe0\x80\x01\x92\x00\x08\xc4L\x00@\x00\x00\x0c\xe0>\xe0\x80\x01\x92\x00\x08\xc4L\x00@\x00\x00\x0c\x00\x01\xc0\x08D\x00\x06\x00\x00\x00\x08\x04\x00\x00\x07\x80\x0f\x00\x00\x00@\x80\x00 \x01\x00\x00\x00\x00\x03\xe0>\x003\x02,\x11\x08\xa4P`\x00\x00\x00\x03\xe0>\x003\x02,\x11\x08\xa4P`\x00\x00\x00\x00\x7f\xf0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
fb = framebuf.FrameBuffer(buffer, WIDTH_LOGO, HEIGHT_LOGO, framebuf.MONO_HLSB)

###################################################################
# Borra todo el Display
oled.fill(1)
oled.show()
time.sleep(2)
oled.fill(0)
oled.show()

# Muestra el Logo
oled.blit(fb, 6, 0)                                       # coordenadas iniciales, centrado --> (x,y) = (6,0)
oled.show()
time.sleep(2)
oled.fill(0)
oled.show()

# Texto de Inicio
oled.text('Iniciando', 12, 0)
oled.text('el', 37, 10)
oled.text('Dispositivo...', 8, 20)
oled.show()
time.sleep(2)
oled.fill(0)
oled.show()

# Ejemplo de barra de intensidad (0 a 100 %)
intensidad = 93
oled.rect(0, 20, WIDTH, 10, 1)                            # dibuja contorno de rectangulo de (x1,y1)=(0,20), ancho=128, espesor=10, color=1
oled.fill_rect(0, 20, int(intensidad*WIDTH/100), 10, 1)   # dibuja rectangulo lleno de (x1,y1)=(0,20), ancho=intensidad (con cambio de escala segun largo del display), espesor=10, color=1            
oled.show()
time.sleep(2)

# Ejemplo de Scroll
oled.fill(0)
oled.show()
contador = 2                                              # cantidad de veces que scrolea
for j in range (contador):
    oled.text('INICIALIZANDO...',0, 10)
    for i in range (0,164):
        oled.scroll(1,0)                                  # scroll 1 pixel a la derecha
        oled.show()
        time.sleep(0.01)

while True:
    oled.text('HOLAAA',0,20)
    for i in range (0,164):
        oled.scroll(1,0)                                  # scroll 1 pixel a la derecha
        oled.show()
        time.sleep(0.01)