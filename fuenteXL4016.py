# Objeto fuenteXL4016 que se utiliza para regular la tensión de salida de una fuente switching basada 
# en el XL4016, a traves de una R variable de 10K Ohm (digital), usando pines de control de la Raspberry Pi Pico

import gpio as GPIO
#from machine import Pin, PWM, ADC


class fuenteXL4016(object):
    def __init__(self, csFuentePin, mosiFuentePin, clkFuentePin):
        self.csFuentePin = csFuentePin
        self.mosiFuentePin = mosiFuentePin
        self.clkFuentePin = clkFuentePin
        self.setupGPIO()
 

    def setupGPIO(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.csFuentePin, GPIO.OUT)
        GPIO.setup(self.mosiFuentePin, GPIO.OUT)
        GPIO.setup(self.clkFuentePin, GPIO.OUT)
     
        
# Funcion que setea el valor de resistencia que se quiere configurar en el potenciometro digital
# En "value" se pasa el valor de R que se necesita: 0 = 0 ohm, 128 = 10K Ohm
    def set_value(self, value):
        GPIO.output(self.csFuentePin, True)
        GPIO.output(self.clkFuentePin, False)
        GPIO.output(self.csFuentePin, False)
        b = '{0:016b}'.format(value)
        for x in range(0, 16):
#           print ('x:' + str(x) + ' -> ' + str(b[x]))
            GPIO.output(self.mosiFuentePin, int(b[x]))
            GPIO.output(self.clkFuentePin, True)
            GPIO.output(self.clkFuentePin, False)
        GPIO.output(self.csFuentePin, True)
        
        
        
