# Objeto intLed que se utiliza para regular la intensidad de in Led usando el PWM y el ADC de 
# la Raspberry Pi Pico

#import gpio as GPIO
from machine import Pin, PWM, ADC


class intLed(object):
    def __init__(self, intensidad, pwmLedPin, adcLedPin, frecuenciaPwm):
        self.potencia = intensidad
        self.pwm = PWM (Pin(pwmLedPin))
        self.adc = ADC (Pin(adcLedPin))
        self.frec = self.pwm.freq(frecuenciaPwm)
#        self.setupGPIO()