# Codigo que permite para automatizar control de raspberry sin usar Thoony
# Tiene 4 funciones: 
# readTemp: mide RTD de la Max31865 y calcula su temperatura. Repite cada "intervalo" de tiempo
# readPot: mide V e I con ADC0 y ADC1 (corriente con ACS712), luego ajusta con polinomios que se ingresan a mano,
# y provienen de calibracion_fuente_graficos.py y calibracion_fuente.py
# setIntensity: controla "intendidad" de un led, prendido "duracion" segundos
# flashLED: prende un led durante un tiempo "miliseconds"

# Se agrega uso del OLED 1306 por I2C (128x32 o 64 pixel), con algunas funciones mas respecto de la version previa
# (temp en Kelvin, horario, etc)
# funcion medir: se le pasa el nro de ADC (0, 1 o 2) y el nro de mediciones a promediar. Devuelve la tension proporcional en ese ADC
# Se recomienda usar el mismo N_veces (variable para el promedio de mediciones) que se uso en la calibracion (200)


#!/usr/bin/python -tt
import time, math
import sys
import gpio as GPIO
import max31865_5
from machine import Pin, I2C, ADC
import intLed
from ssd1306 import SSD1306_I2C              # sirve para el I2C del display (hay otra libreria para manejarlo por SPI)
import framebuf                              # sirve para transformar una imagen en bytes

###############################################################################
# Definicion de funciones
def readTemp():
	# Definicion de Pines para medicion de temperatura para MAX31865
    csMaxPin   = 17                             # SPIO cs para MAX
    misoMaxPin = 16                             # miso = SPIO Tx para MAX
    mosiMaxPin = 19                             # mosi = SPIO Rx para MAX
    clkMaxPin  = 18                             # SPIO clk para MAXcsPin = 17
    # Inicializa medidor de temperatura Max31865
    max = max31865_5.max31865_5(csMaxPin,misoMaxPin,mosiMaxPin,clkMaxPin)
    tempC, resRTD = max.readTemp()                                              # Guarda temperatura y resistencia en 2 variables
    print (tempC, resRTD)
    return (tempC)

def readPot(level):
	# Configuracion de los pines de control de la Raspberry Pi Pico
    SPI_CS_PIN  = 9        # SPIO Cs
    SPI_CLK_PIN = 6        # SPIO CLK
    SPI_SDI_PIN = 8        # mosi = SPIO Rx
    SPI_SDO_PIN = 7        # miso = SPIO Tx (no se usa)
    shutdown = Pin(0, Pin.OUT, Pin.PULL_DOWN)      # Pin de control de apagado del MCP
    shutdown.value(1)                              # inicializo el MCP (prendido)
    # Configuracion de los puertos del Raspberry Pi Pico usando la libreria GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(SPI_CS_PIN, GPIO.OUT)
    GPIO.setup(SPI_CLK_PIN, GPIO.OUT)
    GPIO.setup(SPI_SDI_PIN, GPIO.OUT)
    GPIO.setup(SPI_SDO_PIN, GPIO.IN)     # no se usa
    # Funcion que setea el valor de resistencia que se quiere configurar en el potenciometro digital
    # En "value" se pasa el valor de R que se necesita: 0 = 0 ohm, 255 = 5K Ohm
    def set_value(value):
        GPIO.output(SPI_CS_PIN, True)
        GPIO.output(SPI_CLK_PIN, False)
        GPIO.output(SPI_CS_PIN, False)
        b = '{0:016b}'.format(value)
        for x in range(0, 16):
            GPIO.output(SPI_SDI_PIN, int(b[x]))
            GPIO.output(SPI_CLK_PIN, True)
            GPIO.output(SPI_CLK_PIN, False)
        GPIO.output(SPI_CS_PIN, True)
    set_value(level)
    time.sleep (0.5)  
## SE AGREGO TODO LO ANTERIOR SOLO PARA VALIDAR QUE ESTE MIDIENDO BIEN V e I (LUEGO SE SACARA ESA PARTE)

    divisor_V = 2000 / (10000 + 2000)              # divisor de tension para la medicion de Tension (Vout = Vin . R1/R1+R2)
    conversion_mV = 185                            # mV por Ampere segun datasheet de ACS712
    DEFAULT_OUTPUT_VOLTAGE = 3.3/2                 # tension de salida del sensor cuando no circula corriente
    valor_tension_fuente = (medirADC(0, 200)/ divisor_V)                        # traduce la tension del ADC0 a Tension de la Fuente
    valor_tension_adc1 = ((medirADC(1, 200) - DEFAULT_OUTPUT_VOLTAGE) * 1000)   # convierte a mV la tension diferencial de ADC 1
    valor_corriente_fuente = abs(valor_tension_adc1 / conversion_mV)         # calcula la corriente (en Ampere: 1A = 185 mV) a partir de la tension en mV    
    # Funciones de ajuste, ya habiendo calibrado desde antes
    modelo_y_V = 0.9886 * valor_tension_fuente - 0.1563
    modelo_y_I = 1.6099 * valor_corriente_fuente + 0.1934
#    modelo_y_I = 0.6694 * (valor_corriente_fuente **2) + 0.1621 * valor_corriente_fuente - 0.1433
    potFuente = modelo_y_V * modelo_y_I
    return (potFuente)



def setIntensity(intensidad,duracion):
    # Definicion de Pines para intensidad de Led y Vumetro
    pwmLedPin = 15                              # SPIO PWM para LED
    adcLedPin = 26                              # SPIO ADC para LED
    frecuenciaPwm = 1000                        # Seteo de frecuencia del PWM
    # Inicializa LED al cual se le regulara su intensidad
    LED = intLed.intLed(intensidad,pwmLedPin,adcLedPin,frecuenciaPwm)
    LED.potencia = round(65025*intensidad/100)                                  # 0 apagado, 65025 maxima intensidad
    LED.pwm.duty_u16(LED.potencia)
    print ('Intensidad del Led:', intensidad,'%')                               # imprime intensidad de led de 0 a 100 %
    print ('Prendido:', duracion, 'seg\n')                                      # imprime por cuantos segundos esta prendido el led
    time.sleep(duracion)
    LED.pwm.duty_u16(0)                                                         # apaga el led


def flashLED(miliseconds):
    led = Pin(15, Pin.OUT)
    print ("Prende el LED %d milisegundos\n" % miliseconds)                     # imprime por cuantos milisegundos esta prendido el led
    led.value(1)
    time.sleep_ms(miliseconds)
    led.value(0)


def medirADC(Nro_adc, N_veces):
    mediciones = []
    if Nro_adc == 0:
        adc0 = (Pin(26, Pin.IN))                       # agregado para configurar GP en alta impedancia
        adc0 = ADC(Pin(26))                            # configuracion de Pin para medicion 
        for i in range (N_veces):
            mediciones.append(adc0.read_u16())
            time.sleep_ms(5)
    elif Nro_adc == 1:
        adc1 = (Pin(27, Pin.IN))                       # agregado para configurar GP en alta impedancia
        adc1 = ADC(Pin(27))                            # configuracion de Pin para medicion
        for i in range (N_veces):
            mediciones.append(adc1.read_u16())
            time.sleep_ms(5)
    elif Nro_adc == 2:
        adc2 = (Pin(28, Pin.IN))                       # agregado para configurar GP en alta impedancia
        adc2 = ADC(Pin(28))                            # configuracion de Pin para medicion
        for i in range (N_veces):
            mediciones.append(adc2.read_u16())
            time.sleep_ms(5)
    else:
        print("\nNo se ingreso Nro. de ADC adecuado [0 - 2].")
        return(0)                                      # devuelve cero si no se eligio el ADC 0, 1 o 2
    prom_mediciones = sum(mediciones)/len(mediciones)
    valor_tension_adc = prom_mediciones * 3.3 / 65535           # traduce la lectura del ADC a Tension (en Volts)
    return(valor_tension_adc)
    
    
###############################################################################
    
if __name__ == "__main__":
    # Configuracion de los pines de control de la Raspberry Pi Pico
    I2C_SDA_PIN = machine.Pin(20)               # Pin SDA de RP Pi (I2C0, pin 26, GP20)
    I2C_SCL_PIN = machine.Pin(21)               # Pin SCL de RP Pi (I2C0, pin 27, GP21)
    
    # Dimensiones del Display y Logo
    WIDTH       = 128
    HEIGHT      = 32
    WIDTH_LOGO  = 115
    HEIGHT_LOGO = 32

    # Inicializa I2C y OLED SSD1306
    i2c = machine.I2C(0, sda=I2C_SDA_PIN, scl=I2C_SCL_PIN, freq=300000)     # I2C 0, sda pin, scl pin, freq (entre 200K y 400K)
    oled = SSD1306_I2C(WIDTH, HEIGHT, i2c)

    # Logo
    buffer = bytearray(b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1e@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1e@\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x7f\xfc\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\xe0>\x00\xf0\x01\xe0x<\x7f\xc3\xfe\x03\xc0\x00\x07\x80\x03\x00p\x01\xe0\xf8|\x7f\xe3\xff\x03\xc0\x00\x0c\x04\x01\xc0\xc0\x03\xf0~|`c\x07\x87\xe0\x00\x0c\x04\x01\xc0\xc0\x03\xf0~|`c\x07\x87\xe0\x00\x0c\x1e\x00\xe0p\x03\xb0x|`a\x81\x87`\x00\x18\x07\x80\xe0\xf0\x03\xb0\xf6L\x7f\xc3\x01\x87`\x00\x18\x03\x800p\x06\x1cv\xcc\x7f\xc3\x81\x8c`\x00\x18\x03\x800p\x06\x1cv\xcc\x7f\xc3\x81\x8c`\x00\x10\x07\x800\xc0\x07\xbcv\xcc`c\x01\x8f8\x00p\x07\xc00p\x07\xfc\xf7\xcc`c\x07\x8f\xf8\x00\x10\x1f\xc00\xf0\x1e.w\x8ca\xe3\x97<\xbc\x00p\x1d\xe00\x7f\xdc\x0eq\x8c\x7f\xc3\xff8\x1c\x00p\x1d\xe00\x7f\xdc\x0eq\x8c\x7f\xc3\xff8\x1c\x00\x18<`0KX\x0c\x91\x08Z\x00\x900\x18\x00\x188` \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x18x~\xe0\x00\x00\x00\x00\x00\x00\x00\x00@\x00\x00\x0c\xe0>\xe0\x80\x01\x92\x00\x08\xc4L\x00@\x00\x00\x0c\xe0>\xe0\x80\x01\x92\x00\x08\xc4L\x00@\x00\x00\x0c\x00\x01\xc0\x08D\x00\x06\x00\x00\x00\x08\x04\x00\x00\x07\x80\x0f\x00\x00\x00@\x80\x00 \x01\x00\x00\x00\x00\x03\xe0>\x003\x02,\x11\x08\xa4P`\x00\x00\x00\x03\xe0>\x003\x02,\x11\x08\xa4P`\x00\x00\x00\x00\x7f\xf0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
    fb = framebuf.FrameBuffer(buffer, WIDTH_LOGO, HEIGHT_LOGO, framebuf.MONO_HLSB)   

    # Borra todo el Display
    oled.fill(1)
    oled.show()
    time.sleep(1)
    oled.fill(0)
    oled.show()

    # Muestra el Logo
    oled.blit(fb, 6, 0)                                     # coordenadas iniciales, centrado --> (x,y) = (6,0)
    oled.show()
    time.sleep(2)
    oled.fill(0)
    oled.show()

    contador = 2                                            # cantidad de veces que scrolea
    for j in range (contador):
        oled.text('INICIALIZANDO',0,10)
        for i in range (0,164):
            oled.scroll(1,0)
            oled.show()
            time.sleep(0.01)

    commandLED = "flashLED"
    commandTemp = "readTemp"
    commandSetLED = "setLED"
    while True:
        command = sys.stdin.readline().strip()
        if command.startswith(commandTemp):
## EL FOR SOLO ESTA PARA CAMBIAR EL POTE DIGITAL Y VALIDAR SI ESTA MIDIENDO BIEN. LUEGO SACAR ESA PARTE Y MODIFICAR readPot ()
            for level in range (0, 256, int(256/6)):
                intervalo = float(command.split(",")[1])         # tiempo entre mediciones de temperatura
                temp_med = readTemp()
                pot_med = readPot(level)
                oled.fill(0)
                oled.show()
                oled.text('Temperatura [K]', 10, 0)
                oled.text(str(round(temp_med+273.15,5)), 35, 10) # imprime la Temp en Kelvin
                oled.text('Power [W]:', 0, 20)
                oled.text(str(round(pot_med,2)), 85, 20)         # imprime la Potencia en Watts
                oled.show()
                time.sleep(intervalo)
        elif command.startswith(commandLED):
            miliseconds = int(command.split(",")[1])             # cantidad de milisegundos en que el LED permanece prendido
            oled.fill(0)
            oled.show()
            oled.text('LED ON [mseg]', 15, 0)
            oled.text(str(miliseconds), 40, 10)
            oled.show()
            flashLED(miliseconds)
        elif command.startswith(commandSetLED):
            intensidad = float(command.split(",")[1])           # intensidad a la que prende el LED
            duracion = float(command.split(",")[2])             # cantidad de segundos en que el LED permanece prendido
            oled.fill(0)
            oled.show()
            oled.text('Intensidad [%]', 8, 0)
            oled.text(str(intensidad), 50, 10)
            oled.rect(0, 20, WIDTH, 10, 1)                            # dibuja contorno de rectangulo de (x1,y1)=(0,20), ancho=128, espesor=10, color=1
            oled.fill_rect(0, 20, int(intensidad*WIDTH/100), 10, 1)   # dibuja rectangulo lleno de (x1,y1)=(0,20), ancho=intensidad (con cambio de escala segun largo del display), espesor=10, color=1            
            oled.show()
            setIntensity(intensidad,duracion)
        command = ""

