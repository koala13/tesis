# Manejo de Potenciomentro digital de 5K Ohm, 256 pasos, High Voltage: MCP41HV51_502
# Se setea el valor de R de salida buscado (de 0 a 5K Ohm), a traves de la variable "resistencia", en Ohms
# El valor de Rpote no se corresponde exactamente (es aproximado) con lo que setea el codigo (creo
# que por resistencias parásitas del circuito)
# Se incorpora medicion de Tension (con ADC) y de corriente (con ADC y sensor Hall ACS712-5A)

import time
import gpio as GPIO                     # Es necesario contar con la libreria gpio
from machine import ADC, Pin


# Configuracion de los pines de control de la Raspberry Pi Pico
SPI_CS_PIN = 9         # SPIO Cs
SPI_CLK_PIN = 6        # SPIO CLK
SPI_SDI_PIN = 8        # mosi = SPIO Rx
SPI_SDO_PIN = 7        # miso = SPIO Tx (no se usa)
shutdown = Pin(0, Pin.OUT, Pin.PULL_DOWN)      # Pin de control de apagado del MCP
shutdown.value(1)                              # inicializo el MCP (prendido)

adc0 = (Pin(26, Pin.IN))                       # agregado para configurar GP en alta impedancia
adc0 = ADC(Pin(26))                            # configuracion de Pin para medicion de Tension
adc1 = (Pin(27, Pin.IN))                       # agregado para configurar GP en alta impedancia
adc1 = ADC(Pin(27))                            # configuracion de Pin para medicion de Corriente

# Configuracion de los puertos del Raspberry Pi Pico usando la libreria GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(SPI_CS_PIN, GPIO.OUT)
GPIO.setup(SPI_CLK_PIN, GPIO.OUT)
GPIO.setup(SPI_SDI_PIN, GPIO.OUT)
GPIO.setup(SPI_SDO_PIN, GPIO.IN)     # no se usa

# Funcion que setea el valor de resistencia que se quiere configurar en el potenciometro digital
# En "value" se pasa el valor de R que se necesita: 0 = 0 ohm, 256 = 5K Ohm
def set_value(value):
    GPIO.output(SPI_CS_PIN, True)
    GPIO.output(SPI_CLK_PIN, False)
    GPIO.output(SPI_CS_PIN, False)
    b = '{0:016b}'.format(value)
    for x in range(0, 16):
#        print ('x:' + str(x) + ' -> ' + str(b[x]))
        GPIO.output(SPI_SDI_PIN, int(b[x]))
        GPIO.output(SPI_CLK_PIN, True)
        GPIO.output(SPI_CLK_PIN, False)
    GPIO.output(SPI_CS_PIN, True)

resistencia = 10                                      # valor de R del potenciomentro, en Ohms
print ('Resistencia: ' + str(resistencia) + ' Ohms')   # imprime el valor de resistencia seteado  
level = int(resistencia*256/5000)                      # traduccion a bits internos: 0 = 0 ohm, 255 = 5K Ohm

# el divisor de tension tanto para medir tension como corriente, se calcula con la relacion de resistencias
# pero habria que medirlo, porque sino no se consideran posibles resistencias parasitas, entonces
# difiere lo medido con el tester vs. lo medido con la RP 
divisor_V = 2000 / (10000 + 2000)              # divisor de tension para la medicion de Tension (Vout = Vin . R1/R1+R2)
#divisor_I = 35100 / (35100 + 64700)            # divisor de tension para la medicion de Corriente (Vout = Vin . R1/R1+R2)
conversion_mV = 185                            # mV por Ampere segun datasheet de ACS712

# tension de salida del sensor cuando no circula corriente
DEFAULT_OUTPUT_VOLTAGE = 3.3/2  # sensor Vcc = 5 V, but a voltage divider is used to get 1.65 V from 2.5 V for the sensor out pin

while True:
    set_value(level)
    time.sleep(0.5)
    
    valor_adc0 = adc0.read_u16()
    valor_tension_adc0 = valor_adc0 * 3.3 / 65535                   # traduce la lectura del ADC0 a Tension (en Volts)
    valor_tension_fuente = (valor_tension_adc0 / divisor_V)         # traduce la tension del ADC0 a Tension de la Fuente
    print('ADC 0 (V):' ,valor_adc0, '. Tension: ' , round(valor_tension_fuente,3), 'V')  # imprime el valor del ADC (0 a 65536) y de la tension (en el ADC y en la fuente)

    valor_adc1 = adc1.read_u16()
    valor_tension_adc1 = valor_adc1 * 3.3 / 65535                   # traduce la lectura del ADC1 a Tension (en Volts)
    valor_tension_adc1 = ((valor_tension_adc1 - DEFAULT_OUTPUT_VOLTAGE) * 1000)    # convierte a mV
    valor_corriente_fuente = (valor_tension_adc1 / conversion_mV)   # calcula la corriente (en Ampere: 1A = 185 mV) a partir de la tension en mV    
    print('ADC 1 (I):' ,valor_adc1, '. Corriente:' ,round(valor_corriente_fuente,3), 'A')              # imprime el valor del ADC (0 a 65536) y de la corriente (en la fuente)

    print('----------------------------------------------')
    
    time.sleep(1)
