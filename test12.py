# Con este codigo se puede Medir temperatura, cada un determinado DELTA de tiempo. Tambien se controla un led: intensidad de 0 a 100
# y cuanto tiempo (PERIODO) esta prendido. Tambien se prende un led por XXXX milisegundos (flashled - tiempo ON)
# Toma los valores seteados por linea de comando (correr: python test.py -h para ver las opciones). Con -t mide temperatura, con -f
# flashea el Led, con -l controla el Led (intensidad y periodo prendido). Cada opcion ademas permite setear sus correpondientes
# parámetros adicinoales (ej: duracion, timeON, etc). Si no se ingresan algunos, hay parámetros por default
# La mediciones de tiempo (de maquina), temperatura y RTD se guardan en un archivo Temperaturas.csv

# Hay que conocer la direccion de los ports seriales:
# En Win: Con el comando "mode" en CMD de win se pueden ver los puertos COM. Tiene que estar usado para poder verlo. Aca se uso COM4
# Otro comando de WIN para ver info de los COMs en la terminal CMD: "wmic path Win32_SerialPort"
# En MacOS: ls /dev/*   --> el puerto aparece como /dev/tty.usbmodem****  o /dev/tty.usbserial****
# En MacOS correr el archivo con python3 (con python (2.7) tira errores de encode)

# A veces tira un error en WIN con los ports seriales y Thoony (a veces hay que conectar/desconecatr un USB cualquiera al port 
# o reiniciar la PC, sino queda "utilizado" el port serial o algo similar)

import csv
import serial
import datetime
import time
import argparse


# Configuracion del puerto Serial, para WIN, MacOs o Linux
#Puerto = "/dev/ttyACM0"                                        # puerto serial para Linux/MacOS
#Puerto = "/dev/tty.usbmodem14201"                              # puerto serial para Linux/MacOS
#Puerto = "/dev/tty.usbmodem1411"                               # puerto serial para MacOS (puede ser otro)
Puerto = "COM4"                                                 # puerto serial para WIN

ser = serial.Serial(Puerto, 115200)
ser.reset_output_buffer()
ser.reset_input_buffer()                                     
                                     

print(f'Conectado al puerto serial {Puerto}...\n')

###############################################################################
# Defino las opciones y parámetros a ingresar por teclado, y sus defaults

def parse_args():
    parser = argparse.ArgumentParser(description='Este programa mide temperaturas con una PT100 y un sensor Adafruit MAX 31865, regula intensidad y tiempo prendido de un Led, y flashea otro Led.')
    parser.add_argument('-t', action='store_true', default=False, dest='tempe', help='Mide temperatura.')
    parser.add_argument('-f', action='store_true', default=False, dest='flash', help='Flashea el Led.')    
    parser.add_argument('-l', action='store_true', default=False, dest='led', help='Controla el Led (intensidad y tiempo prendido).')
    parser.add_argument('-d', action='store', dest='delta', type=float, default=1, help='Medicion temperatura: Tiempo DELTA entre mediciones de temperatura, en [seg]. Default: 1 seg.')    
    parser.add_argument('-o', action='store', dest='on', type=int, default=1000, help='Flash Led: Tiempo ON del Led, en [mseg]. Default: 1 seg.')    
    parser.add_argument('-p', action='store', dest='periodo', type=float, default=1, help='Control Led: Duracion PERIODO del Led prendido, en [seg]. Default: 1 seg.')    
    parser.add_argument('-i', action='store', dest='inten', type=float, default=50, help='Control Led: Intensidad del Led [0-100]. Default: 50.')    
    parser.add_argument('--version', action='version', version='%(prog)s 1.3')
    return parser.parse_args()

args = parse_args()


###############################################################################
try:
    if (args.tempe):                                                        # entra al if si se especifico -t
        commandTemp = f'readTemp,{args.delta}\n'
        with open("Temperaturas.csv","w",newline="") as csvfile:            # crea archivo Temperaturas.csv donde se guardaran las mediciones
            fieldnames=["Tiempo","Temperatura","Resistencia"]                             
            writer= csv.DictWriter(csvfile, fieldnames=fieldnames,delimiter=";")
            writer.writeheader()
            read = True
            while read:
#            for i in range (6):                                                     
                ser.write(commandTemp.encode())
                tiempo_interno = int(time.time())                                   # toma el tiempo interno de la medición
                tiempo = datetime.datetime.now()
                result = ser.readline().decode().strip();                           # registra la temperatura medida
                while result != "":                        
                    temp = round ((float(result.split()[0]) + 273.15), 5)           # traduce temperatura a Kelvin
                    res = float(result.split()[1])
                    print(tiempo.strftime("%Y-%m-%d %H:%M:%S %p"),'---> Temperatura:', temp, 'K.')      # Imprime Fecha/Horario y Temperatura medida
#                    print(f'Horario: {tiempo}. Temperatura: {temp} K. Resistencia: {res} Ohms.')    # Imprime Horario, Temperatura y Resistencia medida
#                    print(f'Temperatura: {temp_y_res[0]} degC. Resistencia.: {temp_y_res[1]} ohms.')
                    writer.writerow({"Tiempo":tiempo_interno, "Temperatura":temp, "Resistencia":res})   # se guardan las mediciones en el archivo csv
#                    writer.writerow({"Tiempo":tiempo, "Temperatura":temp_y_res[0], "Resistencia":temp_y_res[1]}) # se guardan las mediciones en el archivo csv
                    csvfile.flush()
                    break
    elif (args.flash):                                                      # entra al elif si se especifico -f
        commandFlashLED = f'flashLED,{args.on}\n'
        read = True
        ser.write(commandFlashLED.encode())
        while read:
            Flash = ser.readline().decode().strip()
            if Flash == "":
                ser.close()
                print("\nSe cerró puerto serial.")
                break
            else:
                print(Flash)  
    elif (args.led):                                                        # entra al elif si se especifico -l
        commandSetLED = f'setLED,{args.inten},{args.periodo}\n'
        read = True
        ser.write(commandSetLED.encode())
        while read:
            Luz = ser.readline().decode().strip()
            if Luz == "":
                print("\nSe cerró puerto serial.")
                ser.close()
                break
            else:
                print(Luz)
    else:                                                                   # si no se especifica parametros -t, -f o -l, sale
        print("\nNo se especificaron los parámetros. Usar -h para mas detalle.")
        ser.close()
        exit()



except KeyboardInterrupt:
    if ser != None:
        print("\nKeyboard interrupt. Cerrando puerto.")
        ser.close()
    exit()                                                      # sale con ctrl-C. Si no se pone esta linea (en WIN al menos), no sale


